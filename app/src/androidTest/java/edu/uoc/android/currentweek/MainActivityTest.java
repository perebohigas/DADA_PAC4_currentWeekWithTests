package edu.uoc.android.currentweek;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static edu.uoc.android.currentweek.ResultActivity.EXTRA_WEEK;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private static Integer currentWeekValue;
    private static Calendar calendar;

    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule = new IntentsTestRule<>(MainActivity.class);

    @Before
    public void setUp(){

        calendar = Calendar.getInstance();
        currentWeekValue = calendar.get(Calendar.WEEK_OF_YEAR);

    }

    @Test
    public void checkRight() {

        // Type value into input field
        onView(withId(R.id.main_ed_week_number)).perform(typeText(String.valueOf(currentWeekValue)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.main_btn_check)).perform(click());

        // Check the intend
        intended(allOf(
                hasComponent(ResultActivity.class.getName()),
                hasExtra(EXTRA_WEEK, currentWeekValue)));

        // Check the message
        onView(withId(R.id.result_tv_message)).check(matches(withText("Right!!")));

    }

    @Test
    public void checkWrong() {

        // Type value into input field
        onView(withId(R.id.main_ed_week_number)).perform(typeText(String.valueOf(currentWeekValue-1)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.main_btn_check)).perform(click());

        // Check the intend
        intended(allOf(
                hasComponent(ResultActivity.class.getName()),
                hasExtra(EXTRA_WEEK, (currentWeekValue-1))));

        // Check the message
        onView(withId(R.id.result_tv_message)).check(matches(withText("You have failed, try again!!")));

    }
}